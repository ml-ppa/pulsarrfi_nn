# PulsarRFI_NN

This repository holds different implementations for recognition of pulsars. The first concept is semantic segmentation using a UNet, source code and examples can be found in this [directory](unet_semantic_segmentation).