#: initialize the imports here
import numpy as np
import cv2
import matplotlib.pyplot as plt
from scipy import signal
from scipy import ndimage
from scipy import interpolate
from tqdm import tqdm
from sklearn.cluster import SpectralClustering
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score

#from fastai.vision.all import *
#from fastai.torch_core import TensorBase
import torchvision.models as torchModels
#import pathlib
import matplotlib.pyplot as plt
import matplotlib as mp
from pathlib import Path

# import numpy
import warnings

warnings.filterwarnings("ignore", category=UserWarning)
import gc

gc.collect()


class HoughTransform:
    """This class deals with classifying pulsar signals based on the line parameters extracted in accumulation matrix after using Hough transform"""

    def __init__(self, img_real: np.array, binary_img: np.array(np.uint8)):
        self.real_img = img_real
        self.img = binary_img.astype(np.uint8)

    def __calculate_border_coor_ofLine__(self, rho, theta):
        coors = []

        y_left_x = int(0 + 1)
        y_left_y = int(
            (-np.cos(theta) / np.sin(theta)) * y_left_x + rho / np.sin(theta)
        )
        if (
            min((y_left_x, y_left_y)) >= 0
            and max((y_left_x, y_left_y)) <= self.real_img.shape[0]
        ):
            coors.append(y_left_x)
            coors.append(y_left_y)

        y_right_x = int(self.real_img.shape[1] - 2)
        y_right_y = int(
            (-np.cos(theta) / np.sin(theta)) * y_right_x + rho / np.sin(theta)
        )
        if (
            min((y_right_x, y_right_y)) >= 0
            and max((y_right_x, y_right_y)) <= self.real_img.shape[0]
        ):
            coors.append(y_right_x)
            coors.append(y_right_y)

        x_top_y = int(0 + 1)
        x_top_x = int(-(x_top_y - rho / np.sin(theta)) * np.tan(theta))
        if (
            min((x_top_x, x_top_y)) >= 0
            and max((x_top_x, x_top_y)) <= self.real_img.shape[0]
        ):
            coors.append(x_top_x)
            coors.append(x_top_y)

        x_bottom_y = int(self.real_img.shape[0] - 2)
        x_bottom_x = int(-(x_bottom_y - rho / np.sin(theta)) * np.tan(theta))
        if (
            min((x_bottom_x, x_bottom_y)) >= 0
            and max((x_bottom_x, x_bottom_y)) <= self.real_img.shape[0]
        ):
            coors.append(x_bottom_x)
            coors.append(x_bottom_y)

        return coors

    def perform_HoughTransformLine(
        self,
        minimum_vote: int = 200,
        rho_resolution: float = 1,
        theta_res: float = np.pi / 180,
        plot_result: bool = True,
        theta_range: float = [80 / 180 * np.pi, 200 / 180 * np.pi],
    ):
        """This method performs Hough transform on a binary image to extract line features

        :param minimum_vote: minimum strength of the line in the accumulation matrix, defaults to 200
        :type minimum_vote: int, optional
        :param rho_resolution: resolution of the Hough parameter rho, defaults to 1
        :type rho_resolution: float, optional
        :param theta_res: resolution of the Hough parameter theta, defaults to np.pi/180
        :type theta_res: float, optional
        :param plot_result: plot results? , defaults to True
        :type plot_result: bool, optional
        :param theta_range: range of lines with specific theta under consideration, defaults to [80 / 180 * np.pi, 200 / 180 * np.pi]
        :type theta_range: float, optional
        :return: tuple of Img_line, rho_list, theta_list
        :rtype: tuple
        """
        self.minimum_vote = minimum_vote
        self.rho_res = rho_resolution
        self.theta_res = theta_res
        Img_line = np.zeros((self.img.shape[0], self.img.shape[1], 3), dtype="uint8")
        # Img_line[:, :, 1] = self.img
        Img_line[:, :, 2] = self.real_img
        lines = cv2.HoughLines(
            self.img, self.rho_res, self.theta_res, self.minimum_vote
        )
        # print(lines[0])
        # print(lines[1])
        rho_list = []
        theta_list = []
        if lines is not None:
            num_lines = lines.shape[0]
            for line_no in range(0, num_lines):
                rho = lines[line_no, 0, 0]
                # rho_list.append(rho)
                theta = lines[line_no, 0, 1]
                # theta_list.append(theta)
                a = np.cos(theta)
                b = np.sin(theta)
                x0 = a * rho
                y0 = b * rho
                # x1 = int(x0 + 1000 * (-b))
                # y1 = int(y0 + 1000 * (a))
                # x2 = int(x0 - 1000 * (-b))
                # y2 = int(y0 - 1000 * (a))

                # x_axis_x = int(rho / np.cos(theta))
                # x_axis_y = 0

                # y_axis_x = 0
                # y_axis_y = int(rho / np.sin(theta))
                # print(f"DEBUG:(x_axis_x, x_axis_y), (y_axis_x, y_axis_y) = {(x1, y1), (x2, y2)}")
                # print(rho,theta)
                if theta > theta_range[0] and theta <= theta_range[1]:
                    coors = self.__calculate_border_coor_ofLine__(rho, theta)
                    # print(f"DEBUG:Calculated coors are = {coors}")
                    # print(f"DEBUG:Selected coors are = {(coors[0], coors[1]), (coors[2], coors[3])}")
                    # cv2.line(Img_line, (x1, y1), (x2, y2), (255, 0, 0), 3)
                    cv2.line(
                        Img_line,
                        (coors[0], coors[1]),
                        (coors[2], coors[3]),
                        (255, 0, 0),
                        3,
                    )

                    rho_list.append(rho)
                    theta_list.append(theta)
                    # print(f"DEBUG: rho,theta {tuple((rho,theta))}")
                # cv2.imwrite('houghlines3.jpg',img)

                if plot_result:
                    plt.imshow(Img_line)
                if plot_result:
                    plt.show()
        else:
            print(f"MESSAGE: No lines detected!")
        Img_line[:, :, 1] = self.img
        # print(f"MESSAGE: No lines detected!")
        return Img_line, rho_list, theta_list

    def __clusterize_in_ParamSpace__(
        self, rhos: list, thetas: list, show_plot: bool = False
    ):
        num_elements = len(rhos)
        rhos = np.array(rhos)
        thetas = np.array(thetas)

        cluter_elements = np.zeros((num_elements, 3))
        cluter_elements[:, 0] = np.array(rhos)
        cluter_elements[:, 1] = np.array(thetas)

        clustering = SpectralClustering(assign_labels="discretize", random_state=0).fit(
            cluter_elements
        )
        labels = clustering.labels_
        labels = labels - labels.min()
        cluter_elements[:, 2] = np.array(labels)
        # print(num_elements)
        # print(clustering.labels_)
        # print(len(clustering.labels_))

        if show_plot:
            fig = plt.figure()
        if show_plot:
            plt.scatter(
                cluter_elements[:, 0], cluter_elements[:, 1], c=clustering.labels_
            )
        if show_plot:
            plt.show()

        return cluter_elements

    def clusterize_in_ParamSpaceV2(
        self, rhos: list, thetas: list, show_plot: bool = False
    ):
        """This method clusters the lines based on Kmeans using the parameter space of Hough transform

        :param rhos: Hough parameter rho
        :type rhos: list
        :param thetas: Hough parameter theta
        :type thetas: list
        :param show_plot: plot the plot?, defaults to False
        :type show_plot: bool, optional
        :return: rhos, theta in first 2 columns and labels of the clusters in the third column
        :rtype: ndarray
        """
        if len(rhos) > 0:
            cluster_elements = np.zeros((len(rhos), 4))
            for idx, rho in enumerate(rhos):
                rho_current = rho
                theta_current = thetas[idx]
                coors = self.__calculate_border_coor_ofLine__(
                    rho_current, theta_current
                )
                pt1 = np.array([coors[0], coors[1]])
                pt2 = np.array([coors[2], coors[3]])
                mid_point = (pt1 + pt2) / 2
                mid_point = mid_point / self.real_img.shape[0]
                cluster_elements[idx, 0:2] = mid_point

                cluster_elements[idx, 2] = (
                    theta_current / np.pi / 2 * 20
                )  # multiplid with 10 to increase the preference of theta deviation
                cluster_elements[idx, 3] = rho_current / self.real_img.shape[0]
                # print(f"DEBUG:cluster_elements: {cluster_elements[idx,:]} ")

            # kmeans = KMeans(init="random", n_clusters=3,n_init=10,max_iter=300,random_state=42)
            # kmeans.fit(cluster_elements)
            # labels = kmeans.labels_
            silhouette_coefficients = []
            # Notice you start at 2 clusters for silhouette coefficient
            print(f"DEBUG: no of elements is {len(rhos)}")
            list_clusters = []
            for k in tqdm(range(2, len(rhos))):
                # print(f"DEBUG: n_clusters=k {k}")
                kmeans = KMeans(
                    init="random",
                    n_clusters=k,
                    n_init=10,
                    max_iter=300,
                    random_state=42,
                )
                kmeans.fit(cluster_elements)
                score = silhouette_score(cluster_elements, kmeans.labels_)
                silhouette_coefficients.append(score)
                list_clusters.append(k)
            # clustering = SpectralClustering(assign_labels='discretize', random_state=0,affinity='rbf').fit(cluster_elements)
            # labels = clustering.labels_
            if silhouette_coefficients == []:
                kmeans = KMeans(
                    init="random",
                    n_clusters=1,
                    n_init=10,
                    max_iter=300,
                    random_state=42,
                )
            else:
                kmeans = KMeans(
                    init="random",
                    n_clusters=list_clusters[
                        silhouette_coefficients.index(max(silhouette_coefficients))
                    ],
                    n_init=10,
                    max_iter=300,
                    random_state=42,
                )
            kmeans.fit(cluster_elements)
            labels = kmeans.labels_
            cluter_elements = np.zeros((len(rhos), 3))
            cluter_elements[:, 0] = np.array(rhos)
            cluter_elements[:, 1] = np.array(thetas)
            cluter_elements[:, 2] = labels
        else:
            cluster_elements = None
        return cluter_elements
    
    def analyze_cluster_types(self,cluster_elements:np.array):
        """This method classify/label the parametric lines detected as ['nothing','brfi','nrfi','pulsar'] based on the hough parameter theta

        :param cluster_elements: clusters of parametric lines detected after clusterization
        :type cluster_elements: np.array
        :return: tuple of signal types of each cluster elements and unique signal type
        :rtype: tuple
        """        
        if cluster_elements.any():
            types = ['nothing','brfi','nrfi','pulsar']
            #type_no = [0,1,2,3]
            type_list = []
            thetas = np.pi - cluster_elements[:, 1]
            for idx,theta in enumerate(thetas):
                if abs(theta)>0 and abs(theta)<=10/180*np.pi:                
                    type_list.append(1)                
                elif(theta>10/180*np.pi and theta<80/180*np.pi):
                    type_list.append(3)
                elif(theta>80/180*np.pi and theta<110/180*np.pi):
                    type_list.append(2)
                else:
                    type_list.append(0)
                #print(f"MESSAGE: Detected {types[type_list[idx]]},since theta is: {theta}")

            unique_types = set(type_list)
        else:
            types = None
        return [types[x] for x in type_list],[types[x] for x in unique_types] 

    def draw_lines(self, cluster_elements: np.array):
        """This method returns the plotted image with colored lines based on cluster index in the third column

        :param cluster_elements: cluster ndarray of [rho,theta,cluster_index]
        :type cluster_elements: np.array
        :return: rgb image with colored clustered Hough line plots
        :rtype: ndarray
        """
        Img_line = np.zeros((self.img.shape[0], self.img.shape[1], 3), dtype="uint8")
        Img_line[:, :, 2] = self.real_img
        cluster_labels = cluster_elements[:, 2]
        cluster_types = np.unique(cluster_labels)
        cluster_types = cluster_types - np.min(cluster_types)

        num_cluster_types = int(cluster_types.max()) + 1
        print(f"MESSAGE: Number of cluster types is { num_cluster_types} ")
        print(f"MESSAGE: NThe types are  {cluster_types} ")
        colors = np.zeros((num_cluster_types, 3), dtype=np.uint8)
        green_array = np.arange(0, 255, step=255 / num_cluster_types, dtype=np.uint8)
        red_array = red_array = 255 - np.arange(
            0, 255, step=255 / num_cluster_types, dtype=np.uint8
        )
        colors[:, 1] = green_array
        colors[:, 0] = red_array

        if cluster_elements is not None:
            num_lines = cluster_elements.shape[0]
            for line_no in range(0, num_lines):
                rho = cluster_elements[line_no, 0]
                # rho_list.append(rho)
                theta = cluster_elements[line_no, 1]
                # theta_list.append(theta)
                a = np.cos(theta)
                b = np.sin(theta)
                x0 = a * rho
                y0 = b * rho
                x1 = int(x0 + 1000 * (-b))
                y1 = int(y0 + 1000 * (a))
                x2 = int(x0 - 1000 * (-b))
                y2 = int(y0 - 1000 * (a))
                # print(rho,theta)
                type_label = int(cluster_elements[line_no, 2])

                # print(f"Debug: type_label is {type_label} ")
                # print(f"Debug: colors is {colors} ")
                # print(f"Debug: The color is {colors[type_label,:]} ")
                color_tuple = (
                    int(colors[type_label, 0]),
                    int(colors[type_label, 1]),
                    int(colors[type_label, 2]),
                )
                # print(f"Debug: The color tuple is { (colors[type_label,:])} ")
                cv2.line(Img_line, (x1, y1), (x2, y2), (color_tuple), thickness=2)

        return Img_line


import torch
import os
from torch import nn
from torch.utils.data import DataLoader, Dataset
from torchvision import datasets


from torch.nn.functional import normalize
import tifffile as tiff
import torchvision.transforms
from torchvision.transforms import functional

from collections import OrderedDict
import numpy as np


class segmentfreqtimeSignalNN:
    """This class deals with training of a predefined UNet and then performing semantic segmentation of the freq-time graph of pulsars"""

    def __init__(self, train_folder_path: Path = None):
        self.train_folder = train_folder_path
        print("MESSAGE: DeepCNN initialized")
        if train_folder_path != None:
            self.train_images = os.path.join(self.train_folder, "images")
        else:
            self.train_images = None
        if train_folder_path != None:
            self.train_masks = os.path.join(self.train_folder, "masks")
        else:
            self.train_masks = None
        if train_folder_path != None:
            self.train_model_path = os.path.join(
                self.train_folder, "model", "trained_DeepCNN_state.pt"
            )
        else:
            self.train_model_path = None
        print(f"MESSAGE: Train directory selected = {self.train_images}")
        print(f"MESSAGE: Mask directory selected = {self.train_masks}")
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        print(f"MESSAGE: Device selected is = {self.device}")
        self.model = self.UNet().to(self.device)

    def show_network_architecture(self):
        """This method shows the UNet architecture on screen"""
        print("MESSAGE: Network Architecture ___________________________________")
        print(self.UNet().eval())
        print("MESSAGE: ________________________________________________________")

    def __prepare_datasets__(self, train_split: float = 0.2, batch_size: int = 4):
        transform_Image = torchvision.transforms.Compose(
            [
                torchvision.transforms.ToTensor(),
                # torchvision.transforms.CenterCrop(255),
                torchvision.transforms.Resize(512, antialias=True),
            ]
        )
        transform_Mask = torchvision.transforms.Compose(
            [
                torchvision.transforms.ToTensor(),
                # torchvision.transforms.CenterCrop(255),
                torchvision.transforms.Resize(512, antialias=True),
                self.__BinarizeTensor__(threshold=0.25),
            ]
        )
        data_set = self.__ImageDataset__(
            img_dir=self.train_images,
            mask_dir=self.train_masks,
            transform=transform_Image,
            transform_mask=transform_Mask,
        )
        train_size = int(train_split * len(data_set))
        test_size = len(data_set) - train_size
        train_dataset, self.test_dataset = torch.utils.data.random_split(
            data_set, [train_size, test_size]
        )
        self.train_dataset = DataLoader(
            dataset=train_dataset, batch_size=batch_size, shuffle=True, pin_memory=True
        )

    def __train_network__(self, num_epochs: int = 5):
        # model = self.UNet()
        self.model.train()  # Set the model to training mode
        loss_noter = []
        epoch_noter = []
        criterion = self.__CustomLoss__()
        optimizer = torch.optim.Adam(self.model.parameters(), lr=0.001)
        for epoch in range(num_epochs):
            for images, masks in tqdm(self.train_dataset):
                images = images.to(self.device)
                masks = masks.to(self.device)

                # Forward pass
                outputs = self.model(images)
                # print(outputs.dtype);print(masks.dtype)
                # print('Shape of output',outputs.shape,'range',(outputs.min(),outputs.max()))
                loss = criterion(outputs, masks)

                # print(f"Epoch [{epoch+1}/{num_epochs}], Loss: {loss:0.4f}")
                # Backward pass and optimization
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()
            loss_noter.append(loss.item())
            epoch_noter.append(epoch)
            print(f"Epoch [{epoch+1}/{num_epochs}], Loss: {loss:0.4f}")

        torch.save(self.model.state_dict(), self.train_model_path)
        return epoch_noter, loss_noter

    def predictions_by_network(self, num_test=4):
        """This method plots the expected and predicted segementation of the test data

        :param num_test: number of plots, defaults to 4
        :type num_test: int, optional
        """
        # self.test_data_path = test_data_localtion
        self.model = self.UNet().to(self.device)
        self.model.load_state_dict(torch.load(self.train_model_path))

        transform_Image = torchvision.transforms.Compose(
            [
                torchvision.transforms.ToTensor(),
                torchvision.transforms.CenterCrop(255),
                torchvision.transforms.Resize(512, antialias=True),
            ]
        )
        # data_set = self.TestDataset(img_dir=test_data_localtion,transform=transform_Image)
        # self.test_dataset = DataLoader(dataset=data_set,batch_size=1, shuffle=True,pin_memory=True)
        tot_test_data = len(self.test_dataset)
        for idx, data_item in tqdm(enumerate((self.test_dataset))):
            # image_batch,mask_batch = data_item
            id_random = np.random.randint(0, tot_test_data, 1)
            image_batch, mask_batch = self.test_dataset.__getitem__(id_random[0])
            image_batch = torch.unsqueeze(image_batch, dim=0)
            # print(f'Shape of image_batch:{image_batch.shape},and idx is {idx}')

            with torch.no_grad():
                pred = self.model(image_batch.to(self.device))
            pred = pred.to("cpu")
            if idx >= num_test:
                break
            fig, (ax0, ax1, ax2) = plt.subplots(1, 3)
            ax0.imshow(image_batch[0, 0, :, :])
            ax0.set_title(f"Pulsar Image")
            ax1.imshow(mask_batch[0, :, :])
            ax1.set_title(f"Mask")
            ax2.imshow(pred[0, 0, :, :])
            ax2.set_title(f"Prediction")
            fig.set_size_inches(10, 30)
            plt.show()

    class __ImageDataset__(Dataset):
        def __init__(self, img_dir, mask_dir, transform=None, transform_mask=None):
            self.img_dir = img_dir
            self.mask_dir = mask_dir
            self.transform = transform
            self.transform_mask = transform_mask

        def __len__(self):
            num_items = len(os.listdir(self.img_dir))
            return num_items

        def __getitem__(self, idx):
            img_path = os.path.join((self.img_dir), "exp" + str(idx) + ".tif")
            mask_path = os.path.join((self.mask_dir), "exp" + str(idx) + ".tif")
            image = np.array(tiff.imread(img_path), np.uint8)
            image = np.expand_dims(image, 2)

            mask = (
                np.array(
                    tiff.imread(
                        mask_path,
                    ),
                    np.uint8,
                )
                * 255
            )
            # mask[mask<125] = 0
            # mask[mask>=125] = 255

            mask = np.expand_dims(mask, 2)
            if self.transform:
                # print(f'DEBUG: image size: {type(image)}')
                image = self.transform(image)
                # print(type(image))
            if self.transform_mask:
                mask = self.transform_mask(mask)
            return image, mask

        def show_img(self, idx):
            """_summary_

            :param idx: _description_
            :type idx: _type_
            """
            image, mask = self.__getitem__(idx)
            image_pil = functional.to_pil_image(image)
            mask_pil = functional.to_pil_image(mask)
            # data_item_list = [image_pil,mask_pil]
            # grid = make_grid(data_item_list)
            fig, (ax0, ax1) = plt.subplots(1, 2)
            im0 = ax0.imshow(image_pil, cmap="gray")
            im1 = ax1.imshow(mask_pil, cmap="inferno")
            ax0.set_title(f"Image No: exp{idx}.tif")
            ax1.set_title(f"Mask No: exp{idx}.tif")
            # fig.colorbar(im0,ax=ax0)
            fig.set_size_inches(9, 18)

    class __TestDataset__(Dataset):
        def __init__(self, img_dir, transform=None):
            self.img_dir = img_dir
            # self.mask_dir = mask_dir
            self.transform = transform
            # self.transform_mask = transform_mask

        def __len__(self):
            num_items = len(os.listdir(self.img_dir))
            return num_items

        def __getitem__(self, idx):
            img_path = os.path.join((self.img_dir), "exp" + str(idx) + ".tif")
            # mask_path = os.path.join((self.mask_dir),'exp'+str(idx)+'.tif')
            image = np.array(tiff.imread(img_path), np.uint8)
            image = np.expand_dims(image, 2)

            # mask = np.expand_dims(mask,2)
            if self.transform:
                # print(f'DEBUG: image size: {type(image)}')
                image = self.transform(image)
                # print(type(image))

            return image

    class __BinarizeTensor__(object):
        def __init__(self, threshold: float = 0.5):
            self.thr = (
                threshold  # input threshold for [0..255] gray level, convert to [0..1]
            )

        def __call__(self, x):
            return (x > self.thr).to(x.dtype)  # do not change the data type

    class UNet(nn.Module):
        """_summary_

        :param nn: _description_
        :type nn: _type_
        """

        def __init__(self, in_channels=1, out_channels=1, init_features=32):
            super(segmentfreqtimeSignalNN.UNet, self).__init__()
            features = init_features
            self.encoder1 = segmentfreqtimeSignalNN.UNet._block(
                in_channels, features, name="enc1"
            )
            self.pool1 = nn.MaxPool2d(kernel_size=2, stride=2)
            self.encoder2 = segmentfreqtimeSignalNN.UNet._block(
                features, features * 2, name="enc2"
            )
            self.pool2 = nn.MaxPool2d(kernel_size=2, stride=2)
            self.encoder3 = segmentfreqtimeSignalNN.UNet._block(
                features * 2, features * 4, name="enc3"
            )
            self.pool3 = nn.MaxPool2d(kernel_size=2, stride=2)
            self.encoder4 = segmentfreqtimeSignalNN.UNet._block(
                features * 4, features * 8, name="enc4"
            )
            self.pool4 = nn.MaxPool2d(kernel_size=2, stride=2)

            self.bottleneck = segmentfreqtimeSignalNN.UNet._block(
                features * 8, features * 16, name="bottleneck"
            )

            self.upconv4 = nn.ConvTranspose2d(
                features * 16, features * 8, kernel_size=2, stride=2
            )
            self.decoder4 = segmentfreqtimeSignalNN.UNet._block(
                (features * 8) * 2, features * 8, name="dec4"
            )
            self.upconv3 = nn.ConvTranspose2d(
                features * 8, features * 4, kernel_size=2, stride=2
            )
            self.decoder3 = segmentfreqtimeSignalNN.UNet._block(
                (features * 4) * 2, features * 4, name="dec3"
            )
            self.upconv2 = nn.ConvTranspose2d(
                features * 4, features * 2, kernel_size=2, stride=2
            )
            self.decoder2 = segmentfreqtimeSignalNN.UNet._block(
                (features * 2) * 2, features * 2, name="dec2"
            )
            self.upconv1 = nn.ConvTranspose2d(
                features * 2, features, kernel_size=2, stride=2
            )
            self.decoder1 = segmentfreqtimeSignalNN.UNet._block(
                features * 2, features, name="dec1"
            )

            self.conv = nn.Conv2d(
                in_channels=features, out_channels=out_channels, kernel_size=1
            )

        def forward(self, x):
            enc1 = self.encoder1(x)
            enc2 = self.encoder2(self.pool1(enc1))
            enc3 = self.encoder3(self.pool2(enc2))
            enc4 = self.encoder4(self.pool3(enc3))

            bottleneck = self.bottleneck(self.pool4(enc4))

            dec4 = self.upconv4(bottleneck)
            dec4 = torch.cat((dec4, enc4), dim=1)
            dec4 = self.decoder4(dec4)
            dec3 = self.upconv3(dec4)
            dec3 = torch.cat((dec3, enc3), dim=1)
            dec3 = self.decoder3(dec3)
            dec2 = self.upconv2(dec3)
            dec2 = torch.cat((dec2, enc2), dim=1)
            dec2 = self.decoder2(dec2)
            dec1 = self.upconv1(dec2)
            dec1 = torch.cat((dec1, enc1), dim=1)
            dec1 = self.decoder1(dec1)
            return torch.sigmoid(self.conv(dec1))
            # return normalize(self.conv(dec1))
            # return nn.Softmax(dim=1)(self.conv(dec1))

        @staticmethod
        def _block(in_channels, features, name):
            return nn.Sequential(
                OrderedDict(
                    [
                        (
                            name + "conv1",
                            nn.Conv2d(
                                in_channels=in_channels,
                                out_channels=features,
                                kernel_size=3,
                                padding=1,
                                bias=False,
                            ),
                        ),
                        (name + "norm1", nn.BatchNorm2d(num_features=features)),
                        (name + "relu1", nn.ReLU(inplace=True)),
                        (
                            name + "conv2",
                            nn.Conv2d(
                                in_channels=features,
                                out_channels=features,
                                kernel_size=3,
                                padding=1,
                                bias=False,
                            ),
                        ),
                        (name + "norm2", nn.BatchNorm2d(num_features=features)),
                        (name + "relu2", nn.ReLU(inplace=True)),
                    ]
                )
            )

    class __CustomLoss__(nn.Module):
        def __init__(self):
            super(segmentfreqtimeSignalNN.__CustomLoss__, self).__init__()

        def forward(self, output, target):
            # target = torch.LongTensor(target)
            # criterion = nn.CrossEntropyLoss()
            # loss = criterion(output, target)
            loss = ((output - target) ** 2).mean()
            mask = target == 9
            high_cost = (loss * mask.float()).mean()
            return loss + high_cost

    @classmethod
    def segmentFreqTime(cls, train_model_file_path: str, image: np.array):
        """This classmethod segments freq-time image of pulsar signals using a trained UNet from train_model_file_path

        :param train_model_file_path: path to the trained model
        :type train_model_file_path: str
        :param image: pulsar 8bit freq-time image
        :type image: ndarray
        :return: segmeneted image (8-bit) of the freq-time graph (not binary)
        :rtype: ndarray
        """
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        model = cls().UNet().to(device)
        model.load_state_dict(torch.load(train_model_file_path))
        transform_image_func = torchvision.transforms.Compose(
            [
                torchvision.transforms.ToTensor(),
                torchvision.transforms.Resize(256, antialias=True),
            ]
        )
        image = transform_image_func(image).to(device)
        image = torch.unsqueeze(image, dim=0)
        with torch.no_grad():
            pred = model(image)

        pred = pred.to("cpu")
        pred_8bit = np.array(pred[0, 0, :, :] * 255, dtype=np.uint8)
        return pred_8bit

    @classmethod
    def train_deepCNN(
        cls, train_folder_path: Path, train_split: float = 0.2, num_epochs: int = 10
    ):
        """This class method is used to train the UNet using the data stored in the training folder as images and masks

        :param train_folder_path: path to the folder containing training data as images and masks
        :type train_folder_path: Path
        :param train_split: fraction of data for training from 0 to 1, defaults to 0.2
        :type train_split: float, optional
        :param num_epochs: number of iterations to train the network over the training set, defaults to 10
        :type num_epochs: int, optional
        """
        deepCNN_obj = cls(train_folder_path)
        deepCNN_obj.__prepare_datasets__(train_split=train_split)
        epoch_no, loss_list = deepCNN_obj.__train_network__(num_epochs=num_epochs)

    @classmethod
    def initializeNN(cls, train_folder_path: Path):
        """This method initializes the segmentfreqtimeSignalNN module

        :param train_folder_path: path to train folder
        :type train_folder_path: Path
        :return: segmentfreqtimeSignalNN object
        :rtype: segmentfreqtimeSignalNN
        """
        deepCNN_obj = cls(train_folder_path)
        return deepCNN_obj


if __name__ == "__main__":
    segmentfreqtimeSignalNN.train_deepCNN(
        train_folder_path=Path("./syn_data/"), num_epochs=10, train_split=0.5
    )

    ##______________________________________________________________________________________________________________________________________________________
    # img_path = Path("C:/Users/tanum/Documents/WorkFolder/HTWBerlin/Curent/digitwinpulsar/code_notebooks/output/images/exp69.tif")
    img_path = Path("./test_data_example/example_P_38580.tif")
    # "C:\Users\tanum\Documents\WorkFolder\HTWBerlin\Curent\digitwinpulsar\output\NoisyPulsarDataCMAP_inferno_example8.png"
    image = np.array(tiff.imread(img_path), np.uint8)
    train_model_file_path = Path("./syn_data/model/trained_DeepCNN_state.pt")
    pred = segmentfreqtimeSignalNN.segmentFreqTime(
        train_model_file_path=train_model_file_path, image=image
    )
    fig, (ax0, ax1) = plt.subplots(1, 2)
    ax0.imshow(image)
    ax1.imshow(pred)
    # plt.imshow(pred)
    plt.show()
    ##_______________________________________________________________________________________________________________________________________________________
    # deepcnn.show_network_architecture()
    # print(deepcnn.UNet().eval())

