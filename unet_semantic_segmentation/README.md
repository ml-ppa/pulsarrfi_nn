# ML-PPA/PulsarRFI_NN/Segmentation

<img src="docs/graphics_folder/logo.png" alt= “PackageLogo” width=20% height=20%>

Welcome to Pulsar Analysis python package Version (0.1). This package in its current form uses the synthetic data generated from digital twin/animation of pulsar freq-time signals in ML-PPA/PulsarDT to train a UNet in to perform semantic segementation of real pulsar freq-time signals away from noise. The idea/motivation behind this is to use legacy machine learning (ML) tools after the  semantic segementation to classify the signals present in the freg-time graph. To start with we used Hough transform to extract parametric line features, which we used later to clusterize based on KMeans. 

## Getting started

### Requirements

This package was developed in a Lenovo Thinkpad with 12th Gen Intel(R) Core(TM) i9-12900H processor with 64 GB of RAM and NVIDIA GeForce RTX 3080 Ti Laptop GPU. Typical usuage uses 20 GB of RAM and 5% of CPU in Microsoft Windows 11. Python dependencies are listed in [requirements.txt](./requirements.txt) file. The python version used is Python 3.10.11.

### Installation
The package can be clonned from the gitlab repository as:
```
git clone https://gitlab-p4n.aip.de/punch/ta5/wp4/ml-ppa/pulsarrfi_nn
```
We recommend using virtual environment for running this package. This can be created in the package folder as:
```
python -m venv <venv_name>
``` 
followed by activating the virtual environment as:
```
source <venv_name>\bin\activate
```
the required packages or dependencies then can be installed in the virtual environment as:
```
pip install -r requirements.txt
```
From here the example python script or the Jupyter Notebooks can be run. You can use the notebooks with a text editor like Visual Studio Code or by running a Jupyter server. To create a custom kernel and run the server use the following:
```
python -m ipykernel install --user --name=<venv_name>
jupyter notebook
```
### Introduction
The package is structured as:
* Pulsar signal analysis: [pulsar_analysis](src/pulsar_analysis/classify_signal.py) 

The synthetic data is stored in this [folder](./syn_data/) as [images](./syn_data/images/) and [masks](./syn_data/masks/) folders. The trained UNet model parameters are stored in this [file](./syn_data/model/trained_DeepCNN_state.pt)

To show the overall implementation of the pipeline run the [main file](./__main__.py) as:
```
python main.py
```
To get a better understanding of different methods used in this packages we also provided some interactive tutorial [Jupyter code notebooks](./examples/). We recommend the users to use this [notebook](./examples/trainDeepCNN.ipynb) specifically to know the details and concept of training of the UNet. 

Detailed doccumentation about the packages is also included in this folder [html doc](./docs/_build/). Please open this [html doc](./docs/_build/html/index.html) in a browser or in a compatible html viewer (recommended: Microsoft Edge browser).

### Implementation
This package is implemented in the ML-PPA pipeline as follows:
1. Synthetic data generation (Please refer to ML-PPA/PulsarDT for details)
2. Model training
3. Test on real data

*_**NOTE**: Only important code blocks are illustrated below. Please do the necessary imports before running the code snippets_*  
**Step 1**: Synthetic data generation: synthetic data can be generated using [synthetic_dataGen]([pulsar_simulation](src/pulsar_simulation/synthetic_dataGen.py)). For details please visit ML-PPA/PulsarDT.

The synthetic data is then stored in the folder in ML-PPA/PulsarDT/syn_data. A copy of this syn_data folder is also placed in this repository as [this](./syn_data/). Below is an example of an image and mask pair:  

<img src="docs/graphics_folder/image_mask_pair.png" alt= “ImageMaskPair” width=50% height=50%> 

**Step 2**: Training the UNet is then executed using the synthetic data by executing the following code below:
```
from pathlib import Path
from src.pulsar_analysis.classify_signal import segmentfreqtimeSignalNN
segmentfreqtimeSignalNN.train_deepCNN(train_folder_path=Path("./syn_data/"),num_epochs=20,train_split=0.5)
```

**Step 3**: Test on real data is illustrated using a fake real pulsar signal as follows:
```
import src.pulsar_analysis.classify_signal as ip
img_example_gray = image
img_mod = pred
Img_line,rho_list,theta_list = ip.HoughTransform(img_real=cv2.resize(img_example_gray,img_mod.shape),binary_img=img_mod).perform_HoughTransformLine(minimum_vote=250,plot_result=False)
cluter_elements = ip.HoughTransform(img_real=cv2.resize(img_example_gray,img_mod.shape),binary_img=img_mod).clusterize_in_ParamSpaceV2(rho_list,theta_list,show_plot=False)
types_detected,unique_types_detected  = ip.HoughTransform(img_real=cv2.resize(img_example_gray,img_mod.shape),binary_img=img_mod).analyze_cluster_types(cluster_elements=cluter_elements)
print('Detected:',unique_types_detected)
```
Executing the above code prints the types of signal present as below:  
```
Detected: ['pulsar']
```

Below is an example of an fake real pulsar segementation followed by clustering:  
*_**NOTE**: Here the segementation process is acheived using a very small training batch. The process can be improved by having a large training batch size with wide range of pulse types. Also we are working on improving the UNet model for better results_*

<img src="docs/graphics_folder/clustering_signals.png" alt= “ImageMaskPair” width=50% height=50%>


## Authors and acknowledgment
**Authors**: Tanumoy Saha   
**Acknowledgment**: We would like to acknowledge PUNCH4NFDI and InterTwin consortium for the funding and the members of TA5 for their valuable support 

## License
Free to use GNU GENERAL PUBLIC LICENSE Version 3.

## Project status
Initial stage of development (Version: 0.1).

