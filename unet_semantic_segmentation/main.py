#: imports
import numpy as np
from pathlib import Path
from src.pulsar_analysis.classify_signal import segmentfreqtimeSignalNN
import tifffile as tiff
import matplotlib.pyplot as plt
import cv2
import src.pulsar_analysis.classify_signal as ip


print(
    "\nMESSAGE:____________________________________________Training____________________________________________"
)
segmentfreqtimeSignalNN.train_deepCNN(
    train_folder_path=Path("./syn_data/"), num_epochs=10, train_split=0.5
)
# segmentfreqtimeSignalNN.train_deepCNN(train_folder_path=Path("../syn_data/"),num_epochs=25,train_split=0.5)
#: Test on fake real pulsar data
img_path = Path("./test_data_example/example_P_38580.tif")
image = np.array(tiff.imread(img_path), np.uint8)
train_model_file_path = Path("./syn_data/model/trained_DeepCNN_state.pt")
pred = segmentfreqtimeSignalNN.segmentFreqTime(
    train_model_file_path=train_model_file_path, image=image
)
pred[pred > 125] = 255
pred[pred <= 125] = 0
fig = plt.figure(figsize=(12, 4))
ax0 = fig.add_subplot(1, 2, 1)
ax1 = fig.add_subplot(1, 2, 2)
# fig,(ax0,ax1) = plt.subplots(1,2)
ax0.imshow(image, cmap="gray")
ax1.imshow(pred, cmap="gray")
plt.show()


#: clusterize and show cluters of lines
print(
    "\nMESSAGE:____________________________________________CLUSTERIZING____________________________________________"
)
img_example_gray = image
img_mod = pred
Img_line, rho_list, theta_list = ip.HoughTransform(
    img_real=cv2.resize(img_example_gray, img_mod.shape), binary_img=img_mod
).perform_HoughTransformLine(minimum_vote=250, plot_result=False)
cluter_elements = ip.HoughTransform(
    img_real=cv2.resize(img_example_gray, img_mod.shape), binary_img=img_mod
).clusterize_in_ParamSpaceV2(rho_list, theta_list, show_plot=False)
if cluter_elements.any() == None:
    print(f"MESSAGE:No Clusters detected as cluter_elements = {cluter_elements}")
Img_line2 = ip.HoughTransform(
    img_real=cv2.resize(img_example_gray, img_mod.shape), binary_img=img_mod
).draw_lines(cluster_elements=cluter_elements)
fig, ax = plt.subplots(1, 4)
fig.set_size_inches(8 * 1.5, 2 * 1.5)
ax[0].imshow(cv2.resize(img_example_gray, img_mod.shape))
ax[1].imshow(Img_line[:, :, 1])
ax[2].imshow(Img_line)
ax[3].imshow(Img_line2)
plt.show()

types_detected,unique_types_detected  = ip.HoughTransform(img_real=cv2.resize(img_example_gray,img_mod.shape),binary_img=img_mod).analyze_cluster_types(cluster_elements=cluter_elements)
print('Detected:',unique_types_detected)
